#------tkinter---------#
import tkinter as tk
from tkinter import ttk

#------mpl&np----------#
import matplotlib as mpl
import numpy as np


Large_Font = ("Verdana", 12)
#----------------------------------Classes----------------------------------------------------------------#
class GraphApp(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        #The container is the frame of the tk, which we inherit from.
        container = tk.Frame(self)
        container.pack(side = "top", fill = "both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        #This dictionary will be populated with the various 'frames' of the app.
        #Whenever we want a certain frame to populate the window, we will call it up using 'tkraise()''.
        self.frames = {}
        frame = StartPage(container, self)
        self.frames[StartPage] = frame
        frame.grid(row=0, column=0, sticky=("nsew"))
        self.show_frame(StartPage)

    #Shows the specified frame by bringing it to the front.
    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

#~---------------------------------------------------------------~#
#Every frame we make will be a class.                            ~#
#Template:                                                       ~#
#class ExPage(Tk.Frame):                                         ~#
#   def __init__(self, parent, controller):                      ~#
#       tk.Frame.__init__(self, parent)                          ~#
#~---------------------------------------------------------------~#

class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text="Test", font = Large_Font)
        label.pack(pady=10, padx=10)


#-----rootdef----------#
app = GraphApp()
app.mainloop()











root.mainloop()
